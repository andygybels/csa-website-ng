import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainResolver } from './shared/resolvers/main.resolver';
import { BaseComponent } from './base/base.component';


const routes: Routes = [{
  path: '',
  component: BaseComponent,
  resolve: {
    userDetail: MainResolver
  },
  loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
