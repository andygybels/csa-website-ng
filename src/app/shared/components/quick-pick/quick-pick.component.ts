import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-quick-pick',
  templateUrl: './quick-pick.component.html',
  styleUrls: ['./quick-pick.component.scss']
})
export class QuickPickComponent implements OnInit {
  @Input() players;
  filteredPlayers: [];
  inputValue: string;
  showPlayerDropdown: boolean;
  selectedPlayers: any[] = [];
  current = 0;
  teamizeClicked = false;
  canReset = false;
  canGo = false;
  @Output() goClicked = new EventEmitter<any[]>();
  @Output() copyClicked = new EventEmitter();
  @Output() resetClicked = new EventEmitter();

  @ViewChild('scrollContainer') scrollContainer: ElementRef;

  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onInputChange(event) {
    this.filterPlayers();
  }

  addPlayer(player) {
    if (this.selectedPlayers.length < 8) {
      this.inputValue = '';
      this.selectedPlayers.push(player);
      this.filterPlayers();
    } else {
      this.toastr.error('You cannot add more than 8 players!', 'Error!', {
        timeOut: 3000,
        progressBar: true,
        positionClass: 'toast-top-center',
      });
    }
  }

  onDeleteClick(player) {
    this.selectedPlayers = this.selectedPlayers.filter(x => x.playerName !== player.playerName);
    this.inputValue = '';
    this.filterPlayers();
  }

  filterPlayers() {
    this.filteredPlayers = this.players
      .filter(x => !this.selectedPlayers.includes(x))
      .filter((obj) => obj.playerShortName.toLowerCase().startsWith(this.inputValue.toLowerCase()));

    this.showPlayerDropdown = !!(this.filteredPlayers && this.filteredPlayers.length && this.inputValue);

    this.canGo = this.selectedPlayers.length > 1;
    this.canReset = this.selectedPlayers.length > 0 || !this.teamizeClicked;

    this.teamizeClicked = false;
  }

  onKeyUp(ev) {
    ev.preventDefault();
    if (this.current > 0 && this.scrollContainer) {
      this.current--;
      this.fixScrolling();
    }
  }

  onKeyDown(ev) {
    ev.preventDefault();
    if (this.filteredPlayers && this.current < this.filteredPlayers.length - 1 && this.scrollContainer) {
      this.current++;
      this.fixScrolling();
    }
  }

  onEnter(ev) {
    if (this.inputValue && this.scrollContainer) {
      this.scrollContainer.nativeElement.scrollTop = 0;
      this.addPlayer(this.filteredPlayers[this.current]);
      this.current = 0;
    } else if (this.selectedPlayers && this.selectedPlayers.length > 1 && !this.teamizeClicked) {
      this.teamizeClicked = true;
      this.goClicked.emit(this.selectedPlayers);
    } else if (this.teamizeClicked) {
      this.onCopyClicked();
    }
  }

  onBackSpace(ev) {
    if (!this.inputValue) {
      ev.preventDefault();
      this.selectedPlayers.pop();
      this.filterPlayers();
    }
  }

  fixScrolling() {
    const liH = 32;
    this.scrollContainer.nativeElement.scrollTop = liH * this.current;
  }


  isActive(index) {
    return this.current === index;
  }

  onGoClick() {
    this.teamizeClicked = true;
    this.goClicked.emit(this.selectedPlayers);
  }

  onResetClick() {
    this.selectedPlayers = [];
    this.filterPlayers();
    this.inputValue = '';
    this.current = 0;
    this.resetClicked.emit();
  }

  onCopyClicked() {
    this.toastr.success('Copied to clipboard!', 'Success!', {
      timeOut: 3000,
      progressBar: true,
      positionClass: 'toast-top-center',
    });

    this.copyClicked.emit();
  }
}
