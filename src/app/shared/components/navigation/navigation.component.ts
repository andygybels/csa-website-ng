import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationComponent {
  @Input() userDetail;
  @Output() logout: EventEmitter<any> = new EventEmitter();

  constructor(private activatedRoute: ActivatedRoute) {
   }

  onLogout() {
    this.logout.next();
  }
}
