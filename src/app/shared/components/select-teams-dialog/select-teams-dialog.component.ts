import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-select-teams-dialog',
  templateUrl: './select-teams-dialog.component.html',
  styleUrls: ['./select-teams-dialog.component.scss']
})
export class SelectTeamsDialogComponent implements OnInit {

  @Input() calculatedTeams: any[];

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {

  }

  onTeamCardClick(team) {
    this.activeModal.close(team);
  }
}
