import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTeamsDialogComponent } from './select-teams-dialog.component';

describe('SelectTeamsDialogComponent', () => {
  let component: SelectTeamsDialogComponent;
  let fixture: ComponentFixture<SelectTeamsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTeamsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTeamsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
