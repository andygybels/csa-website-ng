import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faSignOutAlt, faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  @Input() user;
  @Output() logout: EventEmitter<any> = new EventEmitter();
  faSignOutAlt = faSignOutAlt;
  faSignInAlt = faSignInAlt;

  constructor() { }

  ngOnInit(): void {
  }

  onLogout(): void {
    this.logout.next();
  }
}
