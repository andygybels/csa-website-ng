import { Component, OnDestroy } from '@angular/core';

import { faCheckSquare, faSquare } from '@fortawesome/free-regular-svg-icons';

import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'checkbox-cell',
  template: `
   <fa-icon [icon]="params.value ? faCheckSquare : faSquare"></fa-icon>
  `,
})
export class CheckboxCellRendererComponent implements ICellRendererAngularComp, OnDestroy {
  params: any;
  faCheckSquare = faCheckSquare;
  faSquare = faSquare;

  agInit(params: any): void {
    this.params = params;
  }

  ngOnDestroy() {
    console.log(`Destroying SquareComponent`);
  }

  refresh(): boolean {
    return false;
  }
}
