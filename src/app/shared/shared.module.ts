import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ToLocaleTimeStringPipe } from './pipes/to-locale-time-string.pipe';
import { ToLocaleDateStringPipe } from './pipes/to-locale-date-string.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { FormsModule } from '@angular/forms';
import { QuickPickComponent } from './components/quick-pick/quick-pick.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AgGridModule } from 'ag-grid-angular';
import { CheckboxCellRendererComponent } from './components/checkbox-cell-renderer/checkbox-cell-renderer.component';
import { ClipboardModule } from 'ngx-clipboard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SelectTeamsDialogComponent } from './components/select-teams-dialog/select-teams-dialog.component';
import { TeamSumPipe } from './pipes/team-sum.pipe';

@NgModule({
  declarations: [NavigationComponent, ToLocaleTimeStringPipe, ToLocaleDateStringPipe, UserDetailComponent, QuickPickComponent, CheckboxCellRendererComponent, SelectTeamsDialogComponent, TeamSumPipe],
  imports: [
    CommonModule,
    LazyLoadImageModule,
    RouterModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    DragDropModule,
    ClipboardModule,
    NgbModule

  ],
  exports: [
    NavigationComponent,
    LazyLoadImageModule,
    ToLocaleTimeStringPipe,
    ToLocaleDateStringPipe,
    FontAwesomeModule,
    FormsModule,
    QuickPickComponent,
    DragDropModule,
    CheckboxCellRendererComponent,
    ClipboardModule,
    NgbModule,
    SelectTeamsDialogComponent,
    TeamSumPipe
  ],
  entryComponents: [CheckboxCellRendererComponent]
})
export class SharedModule { }
