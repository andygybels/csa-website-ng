import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { User } from '../models/user';
import { tap, mergeMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.baseUrl;
  phpbbUrl = environment.phpbbUrl;

  public userDetail$ = new BehaviorSubject({});

  constructor(private httpClient: HttpClient) {
  }

  getUserDetail(): Observable<any> {
    return this.session().pipe(mergeMap(resp => this.httpClient.get(this.baseUrl + 'api/me'))).pipe(
      tap(u => {
        this.userDetail$.next(u);
      })
    );
  }

  login(user: User): Observable<any> {
    let body = new HttpParams();
    body = body.append('username', user.username);
    body = body.append('password', user.password);
    body = body.append('persistLogin', user.remember.toString());

    return this.httpClient.post(this.phpbbUrl + 'login', body.toString(), {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  session() {
    return this.httpClient.get(this.phpbbUrl + 'users/session');
  }

  logout() {
    return this.httpClient.post(this.phpbbUrl + 'logout', {});
  }
}
