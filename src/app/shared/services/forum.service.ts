import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ForumService {
  public topics$ = new BehaviorSubject([]);

  constructor(private http: HttpClient) {

  }

  getTopics(): Observable<any> {
    return this.http.get('phpBB3/app.php/restApiV1/forums').pipe(
      tap(u => this.topics$.next(u))
    );
  }
}
