import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamizerService {
  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) { }

  getLogs(): Observable<any[]> {
    return this.httpClient.get<any[]>(this.baseUrl + 'api/logs').pipe(
      map(data => {
        const t = data.map(x => {
          return {
            data: x,
            day: new Date(new Date(x.date).toDateString()),
          };
        });

        const result = _(t)
          .groupBy('day')
          .map((items, day) => {
            return {
              day,
              items: _.map(items, 'data'),
            };
          }).value();

        return result;
      })
    );
  }

  getPlayers(): Observable<any[]> {
    return this.httpClient.get<any[]>(this.baseUrl + 'api/players');
  }

  updatePlayerRate(playerId: number, player): Observable<any> {
    return this.httpClient.post<any>(this.baseUrl + 'api/players/' + playerId, {
      rating: player.rating,
      player_short_name: player.playerShortName
    });
  }
}
