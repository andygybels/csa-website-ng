import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CalculationService {

  constructor() { }


   calculateTeams(arr) {
    var min_diff;
    var result = {
      teamOne: [],
      teamTwo: []
    };

    var teams = [];

    tugOfWar(arr);

    // function that tries every possible solution
    // by calling itself recursively
    function TOWUtil(arr, n, currElements, selectedElements, soln, sum, currSym, currPosition) {
      // checks whether the it is going out of bound
      if (currPosition == n)
        return;

      // checks that the numbers of elements left
      // are not less than the number of elements
      // required to form the solution
      var left = (Math.floor(n / 2) - selectedElements);
      var right = (n - currPosition);
      if (left > right) {
        return;
      }

      // consider the cases when current element
      // is not included in the solution
      TOWUtil(arr, n, currElements, selectedElements, soln, sum, currSym, currPosition + 1);

      // add the current element to the solution
      selectedElements++;
      currSym = currSym + arr[currPosition].rating;
      currElements[currPosition] = true;

      // checks if a solution is formed
      if (selectedElements == Math.floor(n / 2)) {
        // checks if the solution formed is
        // better than the best solution so
        // far

        var min = (sum / 2) - 3;
        var max = min + 6;

        var team = {
          teamOne: [],
          teamTwo: []
        }


        var teamTwo = [];
        for (var i = 0; i < n; i++) {
          if (currElements[i] == true)
            teamTwo.push(arr[i]);
        }

        team.teamTwo = _.orderBy(teamTwo, ['rating'], ['desc']);

        var teamOne = [];
        for (var i = 0; i < n; i++) {
          if (currElements[i] == false)
            teamOne.push(arr[i]);
        }

        team.teamOne = _.orderBy(teamOne, ['rating'], ['desc']);

        var hasTeams = false;
        for (var i = 0; i < teams.length; i++) {
          if (JSON.stringify(teams[i].teamOne) === JSON.stringify(team.teamOne) || JSON.stringify(teams[i].teamOne) === JSON.stringify(team.teamTwo)) {
            hasTeams = true;
          }
        }

        if (!hasTeams) {
          teams.push(team);
        }

        if (Math.abs(sum / 2 - currSym) <
          min_diff) {
          min_diff = Math.abs(sum / 2 -
            currSym);
          for (var i = 0; i < n; i++)
            soln[i] = currElements[i];
        }
      } else {
        // consider the cases where current
        // element is included in the
        // solution
        TOWUtil(arr, n, currElements, selectedElements, soln, sum, currSym, currPosition + 1);
      }

      // removes current element before
      // returning to the caller of this
      // function
      currElements[currPosition] = false;
    }

    // main function that generate an arr
    function tugOfWar(arr) {
      var n = arr.length;

      // the boolen array that contains the
      // inclusion and exclusion of an element
      // in current set. The number excluded
      // automatically form the other set
      var currElements = [];

      // The inclusion/exclusion array for
      // final solution
      var soln = [];

      min_diff = 9007199254740991;

      var sum = 0;
      for (var i = 0; i < n; i++) {
        sum += arr[i].Rating;
        currElements[i] = soln[i] = false;
      }

      // Find the solution using recursive
      // function TOWUtil()
      TOWUtil(arr, n, currElements, 0, soln, sum, 0, 0);

      // var result = {
      //   teamTwo: [],
      //   teamOne: [],
      //   isDefaultTeam: true
      // }

      // var teamTwo = [];
      // for (var i = 0; i < n; i++) {
      //   if (soln[i] == true)
      //     teamTwo.push(arr[i]);
      // }

      // result.teamTwo = _.orderBy(teamTwo, ['Rating'], ['desc']);

      // var teamOne = [];
      // for (var i = 0; i < n; i++) {
      //   if (soln[i] == false)
      //     teamOne.push(arr[i]);
      // }

      // result.teamOne = _.orderBy(teamOne, ['Rating'], ['desc']);

      // teams.push(result);    


    }

    var orderedTeams = _.orderBy(teams, function (e) {
      var sumTeamOne = e.teamOne.reduce((a, b) => ({ rating: a.rating + b.rating })).rating;
      var sumTeamTwo = e.teamTwo.reduce((a, b) => ({ rating: a.rating + b.rating })).rating;
      return Math.abs(sumTeamOne - sumTeamTwo);
    }, ['asc']);

    return _.take(orderedTeams, 5);
  }


  generateTeamString(currentCalculatedTeam): Observable<any> {
    const teamOneString = _.orderBy(currentCalculatedTeam.teamOne, ['rating'], ['desc'])
      .map(t => t.playerShortName + '.' + t.rating).toString().replace(/,/g, ' ');
    const sumTeamOne = currentCalculatedTeam.teamOne.reduce((a, b) => ({ rating: (+a.rating) + (+b.rating) }));

    const teamTwoString = _.orderBy(currentCalculatedTeam.teamTwo, ['rating'], ['desc'])
      .map(t => t.playerShortName + '.' + t.rating).toString().replace(/,/g, ' ');
    const sumTeamTwo = currentCalculatedTeam.teamTwo.reduce((a, b) => ({ rating: (+a.rating) + (+b.rating) }));

    return of(teamOneString + ' <-' + sumTeamOne.rating + ' vs ' + sumTeamTwo.rating + ' -> ' + teamTwoString);
  }
}
