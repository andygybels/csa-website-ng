import { Pipe, PipeTransform } from '@angular/core';
import detectBrowserLanguage from 'detect-browser-language';

const options = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
};

@Pipe({
  name: 'toLocaleTimeString'
})

export class ToLocaleTimeStringPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    return new Date(value * 1000).toLocaleTimeString(detectBrowserLanguage(), options)
  }
}
