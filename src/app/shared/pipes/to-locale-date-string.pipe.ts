import { Pipe, PipeTransform } from '@angular/core';
import detectBrowserLanguage from 'detect-browser-language';

const options = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
};

@Pipe({
  name: 'toLocaleDateString'
})
export class ToLocaleDateStringPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): unknown {
    return new Date(value).toLocaleDateString(detectBrowserLanguage(), options)
  }
}
