import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'teamSum',
  pure: false
})
export class TeamSumPipe implements PipeTransform {

  transform(value: any[], ...args: unknown[]): number {
    if (!value || !value.length) {
      return 0;
    }

    return value.map(x => +x.rating).reduce((x, y) => x + y);
  }
}
