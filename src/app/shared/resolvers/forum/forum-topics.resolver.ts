import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ForumService } from '../../services/forum.service';

@Injectable({ providedIn: 'root' })

export class ForumTopicsResolver implements Resolve<any[]> {
  constructor(private service: ForumService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return this.service.getTopics();
  }
}
