import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TeamizerService } from '../../services/teamizer.service';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class TeamizerResolver implements Resolve<any[]> {
  constructor(private service: TeamizerService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return this.service.getPlayers();
  }
}
