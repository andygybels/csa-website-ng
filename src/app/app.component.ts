import { Component, OnDestroy } from '@angular/core';
import { UserService } from './shared/services/user.service';
import { ForumService } from './shared/services/forum.service';
import { mergeMap, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
