import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { UserService } from '../shared/services/user.service';
import { ForumService } from '../shared/services/forum.service';
import { tap, mergeMap, takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnDestroy {

  title = 'csa-website';
  userDetail;

  private ngUnsubscribe = new Subject();

  constructor(private userService: UserService, private forumService: ForumService, private activatedRoute: ActivatedRoute) {
    this.userService.userDetail$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(x => this.userDetail = x);
  }

  onLogout() {
    this.userService.logout().pipe(
      mergeMap(() => this.userService.getUserDetail())
    ).pipe(takeUntil(this.ngUnsubscribe)).subscribe();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
