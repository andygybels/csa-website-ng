import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntroPageComponent } from './intro-page/intro-page.component';
import { ForumPageComponent } from './forum-page/forum-page.component';
import { GetinvolvedPageComponent } from './getinvolved-page/getinvolved-page.component';
import { JoinusPageComponent } from './joinus-page/joinus-page.component';
import { TeamizerLatestPageComponent } from './teamizer-latest-page/teamizer-latest-page.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { TeamizerLatestResolver } from '../shared/resolvers/teamizer/teamizer-logs.resolver';
import { ForumTopicsResolver } from '../shared/resolvers/forum/forum-topics.resolver';
import { TeamizerComponent } from './teamizer/teamizer.component';
import { TeamizerResolver } from '../shared/resolvers/teamizer/teamizer.resolver';
import { TeamizerAdminComponent } from './teamizer-admin/teamizer-admin.component';
import { TournamentPageComponent } from './tournament-page/tournament-page.component';

const routes: Routes = [
  { path: '', component: IntroPageComponent },
  { path: 'intro', component: IntroPageComponent },
  {
    path: 'forum', component: ForumPageComponent,
    resolve: {
      topics: ForumTopicsResolver
    }
  },
  { path: 'getinvolved', component: GetinvolvedPageComponent },
  { path: 'joinus', component: JoinusPageComponent },
  {
    path: 'teamizer-changes', component: TeamizerLatestPageComponent,
    resolve: {
      logs: TeamizerLatestResolver
    }
  },
  {
    path: 'teamizer', component: TeamizerComponent,
    resolve: {
      players: TeamizerResolver
    }
  },
  {
    path: 'teamizer-admin', component: TeamizerAdminComponent,
    resolve: {
      players: TeamizerResolver
    }
  },
  { path: 'login', component: LoginPageComponent },
  { path: 'tournament', component: TournamentPageComponent },
  { path: '**', component: NotFoundPageComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
