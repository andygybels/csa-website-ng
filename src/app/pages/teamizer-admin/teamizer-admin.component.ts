import { Component, OnInit } from '@angular/core';
import { HostListener } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { GridApi, ColDef } from 'ag-grid-community';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { CheckboxCellRendererComponent } from 'src/app/shared/components/checkbox-cell-renderer/checkbox-cell-renderer.component';
import { TeamizerService } from 'src/app/shared/services/teamizer.service';


@Component({
  templateUrl: './teamizer-admin.component.html',
  styleUrls: ['./teamizer-admin.component.scss']
})
export class TeamizerAdminComponent implements OnInit {
  private gridApi: GridApi;
  private gridColumnApi;
  screenHeight: number;
  screenWidth: number;
  players = [];
  faCoffee = faCoffee;
  selectedPlayer;
  getRowNodeId;
  error = false;

  columnDefs: ColDef[] = [
    { headerName: 'Player', field: 'playerName', sortable: true, filter: 'agTextColumnFilter' },
    { headerName: 'Short name', field: 'playerShortName', sortable: true, filter: 'agTextColumnFilter', sort: 'asc' },
    { headerName: 'Rating', field: 'rating', sortable: true, filter: 'agNumberColumnFilter', },
    { headerName: 'Has rating', field: 'playerHasRating', cellRendererFramework: CheckboxCellRendererComponent},
  ];

  constructor(private route: ActivatedRoute, private teamizerService: TeamizerService) {
    this.getScreenSize();
    this.players = this.route.snapshot.data.players;

    this.getRowNodeId = (data) => {
      return data.playerId;
    };
  }

  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    console.log(this.screenHeight, this.screenWidth);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  onSelectionChanged() {
    this.error = false;
    this.selectedPlayer = this.gridApi.getSelectedRows()[0];
  }

  onSaveClick() {

    if (!this.selectedPlayer || !this.selectedPlayer.rating || !this.selectedPlayer.playerShortName) {
      this.error = true;
    }else{
      this.error = false;
      this.teamizerService.updatePlayerRate(this.selectedPlayer.playerId, this.selectedPlayer).subscribe((player) => {
        const playerInList = this.players.find(x => x.playerId === player.playerId);
        playerInList.rating = player.rating;
        playerInList.playerHasRating = player.playerHasRating;
  
        const rowNode = this.gridApi.getRowNode(player.playerId);
        rowNode.setData(player);
      });
    }  
  }
}
