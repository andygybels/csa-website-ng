import { Component, OnInit, OnDestroy } from '@angular/core';

import { faChevronCircleUp, faChevronCircleDown } from '@fortawesome/free-solid-svg-icons';
import { TeamizerService } from 'src/app/shared/services/teamizer.service';
import * as _ from 'lodash';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './teamizer-latest-page.component.html',
  styleUrls: ['./teamizer-latest-page.component.scss']
})
export class TeamizerLatestPageComponent {
  faChevronCircleUp = faChevronCircleUp;
  faChevronCircleDown = faChevronCircleDown;
  teamizerLogs = [];

  constructor(private route: ActivatedRoute) {
    this.teamizerLogs = this.route.snapshot.data.logs;
  }
}
