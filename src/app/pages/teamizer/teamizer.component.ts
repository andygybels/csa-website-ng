import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CalculationService } from 'src/app/shared/services/calculation.service';

import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag } from '@angular/cdk/drag-drop';
import { ClipboardService } from 'ngx-clipboard';
import * as _ from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectTeamsDialogComponent } from 'src/app/shared/components/select-teams-dialog/select-teams-dialog.component';

@Component({
  templateUrl: './teamizer.component.html',
  styleUrls: ['./teamizer.component.scss']
})
export class TeamizerComponent implements OnInit {
  players = [];
  calculatedTeams = [];
  currentCalculatedTeam;

  constructor(
    private route: ActivatedRoute,
    private calculationService: CalculationService,
    private clipboard: ClipboardService,
    private modalService: NgbModal
  ) {
    this.players = this.route.snapshot.data.players.filter(x => x.playerHasRating);
    this.currentCalculatedTeam = {
      teamOne: [],
      teamTwo: []
    };

    for (let i = 1; i <= 50; i++) {
      this.players.push({
        playerName: 'Sub' + i,
        playerShortName: 'Sub' + i,
        rating: i
      });
    }
  }

  ngOnInit(): void {
  }

  onGoClicked(event) {
    this.calculatedTeams = this.calculationService.calculateTeams(event);
    this.setCurrentTeam(this.calculatedTeams[0]);
  }

  setCurrentTeam(teams) {
    this.currentCalculatedTeam = teams;
  }

  onResetClicked() {
    this.calculatedTeams = [];
    this.currentCalculatedTeam = {
      teamOne: [],
      teamTwo: []
    };
  }

  onCopyClicked() {
    if (!this.currentCalculatedTeam.teamOne.length || !this.currentCalculatedTeam.teamTwo.length) {
      return;
    }

    this.calculationService.generateTeamString(this.currentCalculatedTeam).subscribe(x => {
      this.clipboard.copy(x);
    });
  }

  canTransferItem() {
    console.log(this.currentCalculatedTeam.teamTwo.length)
    return this.currentCalculatedTeam.teamOne &&
      this.currentCalculatedTeam.teamOne.length &&
      this.currentCalculatedTeam.teamOne.length > 1 &&
      this.currentCalculatedTeam.teamTwo &&
      this.currentCalculatedTeam.teamTwo.length &&
      this.currentCalculatedTeam.teamTwo.length > 1;
  }

  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  canDropPredicate(event: CdkDrag<any>) {
    const data = event.data;
    return data && data.length > 1;
  }

  openSelectTeamsDialog() {
    const modalRef = this.modalService.open(SelectTeamsDialogComponent, { size: 'lg' });
    modalRef.componentInstance.calculatedTeams = this.calculatedTeams;
    modalRef.result.then(x => {
      this.currentCalculatedTeam = x;
    }).catch(x => { });
  }

  trackByPlayerId(index: number, player: any): string {
    return player.playerId;
  }
}
