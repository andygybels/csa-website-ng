import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/shared/models/user';
import { UserService } from 'src/app/shared/services/user.service';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {
  user: User;
  private ngUnsubscribe = new Subject();

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.user = {
      remember: false,
      password: '',
      username: ''
    };
  }

  onFormSubmit(event) {
    this.userService.login(this.user).pipe(
      mergeMap(() => this.userService.getUserDetail())
    ).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      () => {
        this.router.navigate(['/forum']);
      }
    );
    event.preventDefault();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
