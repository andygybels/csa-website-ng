import { Component, OnInit, OnDestroy } from '@angular/core';
import { ForumService } from 'src/app/shared/services/forum.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './forum-page.component.html',
  styleUrls: ['./forum-page.component.scss']
})
export class ForumPageComponent implements OnInit, OnDestroy {
  forums = [];
  private ngUnsubscribe = new Subject();

  constructor(private route: ActivatedRoute, private forumService: ForumService) {
  }

  ngOnInit(): void {
    this.forumService.topics$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(x => {
      this.forums = x;
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
