import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { IntroPageComponent } from './intro-page/intro-page.component';
import { JoinusPageComponent } from './joinus-page/joinus-page.component';
import { ForumPageComponent } from './forum-page/forum-page.component';
import { GetinvolvedPageComponent } from './getinvolved-page/getinvolved-page.component';
import { TeamizerLatestPageComponent } from './teamizer-latest-page/teamizer-latest-page.component';
import { SharedModule } from '../shared/shared.module';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { TeamizerComponent } from './teamizer/teamizer.component';
import { TeamizerAdminComponent } from './teamizer-admin/teamizer-admin.component';
import { AgGridModule } from 'ag-grid-angular';
import { TournamentPageComponent } from './tournament-page/tournament-page.component';


@NgModule({
  declarations: [
    IntroPageComponent,
    JoinusPageComponent,
    ForumPageComponent,
    GetinvolvedPageComponent,
    TeamizerLatestPageComponent,
    NotFoundPageComponent,
    LoginPageComponent,
    TeamizerComponent,
    TeamizerAdminComponent,
    TournamentPageComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
    AgGridModule.withComponents([])

  ]
})
export class PagesModule { }
