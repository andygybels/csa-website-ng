export const environment = {
  production: true,
  baseUrl: '/public/',
  phpbbUrl: '/phpBB3/app.php/restApiV1/'
};
